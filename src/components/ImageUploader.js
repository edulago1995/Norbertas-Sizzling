import React, { useState } from 'react';

const ImageUploader = ({ onImageUpload }) => {
    const API_KEY = '590a3ca0ae7ce73d2b378afedbf1bec0'; // Replace with your ImgBB API Key

    const [uploaded, setUploaded] = useState(false);

    const handleUpload = async () => {
        const formData = new FormData();
        const fileInput = document.getElementById('image-file');

        if (fileInput && fileInput.files.length > 0) {
            formData.append('image', fileInput.files[0]);

            const response = await fetch('https://api.imgbb.com/1/upload?key=' + API_KEY, {
                method: 'POST',
                body: formData,
            });

            const data = await response.json();

            if (data.data) {
                const imageUrl = data.data.url;

                // Call the callback function to set the image URL in the parent component's state
                onImageUpload(imageUrl);
            }
        }
    };

    return (
        <div>
            <input type="file" id="image-file" />
            <button
                id="upload-button"
                onClick={handleUpload}
                disabled={uploaded} // Disable the button if an image has already been uploaded
            >
                {uploaded ? 'Image Uploaded' : 'Upload Image'}
            </button>
        </div>
    );
};

export default ImageUploader;
