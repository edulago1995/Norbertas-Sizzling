import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate   } from 'react-router-dom';
import { Table, Container, Col, Row, Button } from 'react-bootstrap';
import AdminUser from './AdminUser';

export default function AllUsers({ usersData, fetchData }) {
  const [users, setUsers] = useState([]);

  const navigate = useNavigate(); 
  const { user } = useContext(UserContext);

  useEffect(() => {
    // Check if usersData is defined and is an array before mapping over it
    if (Array.isArray(usersData) && usersData.length > 0) {
      const usersArr = usersData.map((user) => (
        <tr key={user._id}>
          <td>{user._id}</td>
          <td className="text-center">{user.firstName}</td>
          <td className="text-center">{user.lastName}</td>
          <td className="text-center">{user.email}</td>
          <td className={user.isAdmin ? 'text-success text-center' : 'text-danger text-center'}>
            {user.isAdmin ? 'admin' : 'regular'}
          </td>
          <td style={{ textAlign: 'center' }}>
            <AdminUser user={user._id} isAdmin={user.isAdmin} fetchData={fetchData} />
          </td>
        </tr>
      ));

      setUsers(usersArr);
    } else {
      // If usersData is undefined or an empty array, set users to an empty array
      setUsers([]);
    }
  }, [usersData, fetchData]);
  console.log(user)
  

  return user.isAdmin === true ? (
    <>
      <Container>
        <Row>
          <Col>
            <h2 className="my-3 py-2">Admin Dashboard - ALL USERS</h2>
          </Col>
          <Col className="text-end my-4">
            <div>
              <Link to="/products">
                <Button variant="primary">All products</Button>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{users}</tbody>
      </Table>
    </>
  ) : (
    (() => {
      navigate('/products');
      return null;
    })()
  );
};
