import { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';


import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
			<Container fluid>
			    <Navbar.Brand as={Link} to="/">
			        <a href="https://imgbb.com/" style={{ textDecoration: 'none' }}>
				        <img
				          src="https://i.ibb.co/44GQtY5/350527252-1110909973631682-1129322120803573381-n-removebg-preview-removebg-preview.png"
				          alt="350527252-1110909973631682-1129322120803573381-n-removebg-preview-removebg-preview"
				          border="0"
				          style={{ width: '50px', height: 'auto' }} // Adjust the width here
				        />
				    </a>
			    </Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ms-auto">

				        {(user.id !== null) ? 

								user.isAdmin 
								?
								<>
									<Nav.Link as={NavLink} to="/products" exact>Admin Dashboard</Nav.Link>
									<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={NavLink} to="/products" exact>Home</Nav.Link>
									<Nav.Link as={NavLink} to="/cart" exact>Cart</Nav.Link>
									<Nav.Link as={NavLink} to="/MyPurchase" exact>My Purchase</Nav.Link>
									<Nav.Link as={Link} to="/profile">Profile</Nav.Link>
									<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
							: 
								<>
									<Nav.Link as={NavLink} to="/products" exact>Home</Nav.Link>
									<Nav.Link as={Link} to="/login">Login</Nav.Link>
								</>
						}
				    </Nav>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
		)
}