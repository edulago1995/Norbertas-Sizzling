import '../App.css'; // Import the CSS file

function CustomNumberInput({ quantity, setQuantity }) {
  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  return (
    <div className="custom-number-input">
      <button onClick={handleDecrement}>&#9666;</button>
      <input
        type="text"
        id="quantity"
        value={quantity}
        onChange={(e) => setQuantity(e.target.value)}
        min={1}
        readOnly 
      />
      <button onClick={handleIncrement}>&#9656;</button>
    </div>
  );
}

export default CustomNumberInput;
