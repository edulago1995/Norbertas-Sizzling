import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Container, Col, Row, Button } from 'react-bootstrap';

import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productsArr = productsData.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product._id}</td>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>PhP {product.price}</td>
          <td className={product.isActive ? 'text-success' : 'text-danger'}>
            {product.isActive ? 'Available' : 'Unavailable'}
          </td>
          <td> <EditProduct product={product._id} fetchData={fetchData} /> </td>
          <td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} /></td>
        </tr>
      );
    });

    setProducts(productsArr);
  }, [productsData]);

  return (
  <>
    <Container>
      <Row>
        <Col>
          <h2 className="my-3 py-2">Admin Dashboard - ALL PRODUCTS</h2>
        </Col>
        <Col className="text-end my-4">
          <div>
            <Link to="/users">
              <Button variant="primary" className="me-2">All Users</Button>
            </Link>
            <Link to="/allorders">
              <Button variant="primary" className="me-2">All Orders</Button>
            </Link>
            <Link to="/addProduct">
              <Button variant="primary">Add products</Button>
            </Link>
          </div>
        </Col>
      </Row>
    </Container>
    <Table striped bordered hover responsive>
      <thead>
        <tr className="text-center">
          <th>ID</th>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Availability</th>
          <th colSpan="2">Actions</th>
        </tr>
      </thead>

      <tbody>{products}</tbody>
    </Table>
  </>
);

}
