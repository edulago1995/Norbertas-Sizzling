import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Container, Col, Row, Button } from 'react-bootstrap';
import CancelOrder from '../components/CancelOrder';
import "../App.css";

function DeliveredOrder({ ordersData, fetchData }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`https://nortertas-sizzling.onrender.com/orders/myorders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        // console.log("Data from API:", data); // Check the data received from the API
        if (Array.isArray(data) && data.length > 0) {
          setOrders(data);
        }
      });
  }, [ordersData, fetchData]);

  // console.log("Final Orders:", orders); // Debugging: Check the orders just before rendering

  return (
  <div>
    <h1 id="receipt-header" className="text-center my-3">
      My Purchase
    </h1>
    <div className="d-flex justify-content-center">
            <Link to="/MyPurchase">
              <Button variant="primary" className="me-2">Pending Orders</Button>
            </Link>
            <Link to="/CanceledOrders">
              <Button variant="primary" className="me-2">Canceled Orders</Button>
            </Link>
    </div>
    {orders
      .filter(order => !order.isActive && !order.isPending)
      .sort((a, b) => new Date(b.orderedProducts[0].purchasedOn) - new Date(a.orderedProducts[0].purchasedOn))
      .map((order, orderIndex) => (
        <div key={orderIndex} className="receipt-card">
          <td className="text-success text-center">
              <strong>DELIVERED</strong>
          </td>
          <div className="receipt-content">
            <table id="receipt-table">
            <thead>
              <tr>
                <th id="table-header-name">Product Name</th>
                <th id="table-header-quantity" className="text-right">
                  Quantity
                </th>
                <th id="table-header-price" className="text-right">
                  Price
                </th>
              </tr>
            </thead>
            <tbody>
              {order.orderedProducts.map((productSet, productIndex) => (
                <React.Fragment key={productIndex}>
                  {productSet.products.map((product, innerProductIndex) => (
                    <tr key={innerProductIndex}>
                      <td id="table-header-name">{product.name}</td>
                      <td id="table-header-quantity" className="text-right">
                        {product.quantity}
                      </td>
                      <td id="table-header-price" className="text-right">
                        Php {product.price*product.quantity}
                      </td>
                    </tr>
                  ))}
                </React.Fragment>
              ))}
            </tbody>
          </table>
            <div className="order-info">
              {order.orderedProducts.map((productSet, productSetIndex) => (
                <div key={productSetIndex} className="product-set">
                  <h3 id="total-amount">Total Amount: Php {productSet.totalAmount}</h3>
                  <p>
                    Purchased On: {new Date(productSet.purchasedOn).toLocaleString('en-US', {
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric',
                      hour: 'numeric',
                      minute: 'numeric',
                      second: 'numeric',
                    })}
                  </p>
                </div>
              ))}
              <CancelOrder user={order._id} isPending={order.isPending} isActive={order.isActive} />
            </div>
          </div>
        </div>
      ))}
  </div>
);
}

export default DeliveredOrder;