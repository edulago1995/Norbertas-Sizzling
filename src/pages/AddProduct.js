import {useState,useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import ImageUploader from '../components/ImageUploader'

export default function AddProduct(){

	const navigate = useNavigate();
    const {user} = useContext(UserContext);

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");
	const [img,setImg] = useState("");

	const handleImageUpload = (imageUrl) => {
        setImg(imageUrl);
    };

	function createProduct(e){

		e.preventDefault();

		if (!name || !description || !price || !img) {
		    // Display an error message to the user or handle the missing fields appropriately.
		    Swal.fire({
			  title: 'Please Wait.... uploading image',
			  showClass: {
			    popup: 'animate__animated animate__fadeInDown'
			  },
			  hideClass: {
			    popup: 'animate__animated animate__fadeOutUp'
			  }
			})
		    return;
		}

		let token = localStorage.getItem('token');
		console.log(token);

		fetch(`https://nortertas-sizzling.onrender.com/products/addProducts`,{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
				img: img

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({

					icon:"success",
					title: "Product Added"

				})

				navigate("/products");
			} else {
				Swal.fire({

					icon: "error",
					title: "Unsuccessful Product Creation",
					text: data.message

				})
			}

		})

        setName("");
        setDescription("");
        setPrice(0);
        setImg("");
	}

	return (

            (user.isAdmin === true)
            ?
            <>
                <h1 className="my-5 text-center">Add Prodcut</h1>
                <Form onSubmit={e => createProduct(e)}>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group className="my-2">
		            <ImageUploader onImageUpload={handleImageUpload} />
		            <Form.Control
		              className="my-2"
		              type="text"
		              placeholder="Upload Image"
		              required
		              value={img}
		              onChange={(e) => setImg(e.target.value)}
		              disabled
		            />
			        </Form.Group>
			        <Button
					  variant="primary"
					  type="submit"
					  className="my-5"
					  disabled={!name || !description || !price || !img}
					>
					  Submit
					</Button>
       			</Form>
		    </>
            :
            <Navigate to="/products" />
	)
}